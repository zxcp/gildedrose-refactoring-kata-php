## Запуск проекта
* `start.bat` - для запуска проекта в windows
* `./start.sh` - для запуска в unix

## Консоль контейнера
`docker exec -it fpm sh` - выполняется из папки docker

## Запуск тестов
Выполняется из папки source
* `pu` - windows
* `./pu.sh` - unix