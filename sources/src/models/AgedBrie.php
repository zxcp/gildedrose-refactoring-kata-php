<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose\models;

class AgedBrie extends Provider
{
    public const NAME = 'Aged Brie';

    public function updateQuality(): void
    {
        $this->increaseQuality();
        $this->decreaseSellIn();
        if ($this->isSellInLessThanZero()) {
            $this->increaseQuality();
        }
    }
}
