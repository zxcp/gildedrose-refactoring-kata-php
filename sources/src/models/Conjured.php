<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose\models;

class Conjured extends Provider
{
    public const NAME = 'Conjured Mana Cake';

    public const QUALITY_STEP = 2;

    public function updateQuality(): void
    {
        $this->decreaseQuality();
        $this->decreaseSellIn();
        if ($this->isSellInLessThanZero()) {
            $this->decreaseQuality();
        }
    }
}
