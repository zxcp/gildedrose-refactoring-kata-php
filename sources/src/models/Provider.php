<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose\models;

use GildedRose\Item;

abstract class Provider
{
    // Публичными сделаны для тестов.
    public const MAX_QUALITY = 50;

    public const MIN_QUALITY = 0;

    public const SELL_IN_STEP = 1;

    public const QUALITY_STEP = 1;

    protected Item $item;

    public function __construct(Item &$item)
    {
        $this->item = $item;
    }

    abstract public function updateQuality(): void;

    protected function decreaseSellIn(): void
    {
        $this->item->sell_in -= static::SELL_IN_STEP;
    }

    protected function increaseQuality(): void
    {
        $this->item->quality += static::QUALITY_STEP;
        // Можно и дальше по функциям разбрасывать, но вроде и так всё понятно. DRY соблюдён.
        // А то имена функций будут длинные
        if ($this->item->quality > static::MAX_QUALITY) {
            $this->item->quality = static::MAX_QUALITY;
        }
    }

    protected function decreaseQuality(): void
    {
        $this->item->quality -= static::QUALITY_STEP;
        if ($this->item->quality < self::MIN_QUALITY) {
            $this->item->quality = self::MIN_QUALITY;
        }
    }

    protected function isSellInLessThanZero(): bool
    {
        return $this->item->sell_in < 0;
    }
}
