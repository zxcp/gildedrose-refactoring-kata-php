<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose\models;

use GildedRose\Item;

class Sulfuras extends Provider
{
    public const NAME = 'Sulfuras, Hand of Ragnaros';

    public const MAX_QUALITY = 80;

    public function __construct(Item &$item)
    {
        parent::__construct($item);
        // Легендарный товар «Sulfuras» имеет качество 80 и оно никогда не меняется
        $this->item->quality = self::MAX_QUALITY;
    }

    public function updateQuality(): void
    {
        $this->increaseQuality();
        if ($this->isSellInLessThanZero()) {
            $this->increaseQuality();
        }
    }

    protected function increaseQuality(): void
    {
        //do nothing
    }

    protected function decreaseQuality(): void
    {
        //do nothing
    }
}
