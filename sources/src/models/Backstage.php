<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose\models;

class Backstage extends Provider
{
    public const NAME = 'Backstage passes to a TAFKAL80ETC concert';

    private const SELL_IN_ELEVEN = 11;

    private const SELL_IN_SIX = 6;

    public function updateQuality(): void
    {
        $this->increaseQuality();
        if ($this->item->sell_in < self::SELL_IN_ELEVEN) {
            $this->increaseQuality();
        }
        if ($this->item->sell_in < self::SELL_IN_SIX) {
            $this->increaseQuality();
        }
        $this->decreaseSellIn();
        if ($this->isSellInLessThanZero()) {
            $this->item->quality = self::MIN_QUALITY;
        }
    }
}
