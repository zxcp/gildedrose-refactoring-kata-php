<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose\models;

class Dexterity extends Provider
{
    public const NAME = '+5 Dexterity Vest';

    public function updateQuality(): void
    {
        $this->decreaseQuality();
        $this->decreaseSellIn();
        if ($this->isSellInLessThanZero()) {
            $this->decreaseQuality();
        }
    }
}
