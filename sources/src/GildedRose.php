<?php

declare(strict_types=1);

namespace GildedRose;

use Exception;

final class GildedRose
{
    /**
     * @var Item[]
     */
    private array $items;

    /**
     * GildedRose constructor.
     * @param Item[] $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @throws Exception
     */
    public function updateQuality(): void
    {
        foreach ($this->items as $item) {
            (new ItemsFactory())->build($item)
                ->updateQuality();
        }
    }

//    /**
//     * Функция для дебага
//     * @param mixed $data
//     * @param bool $exit
//     */
//    public function dd($data, bool $exit = true): void
//    {
//        switch (true) {
//            case is_array($data):
//            case is_object($data):
//                echo '<pre>';
//                print_r($data);
//                echo '</pre>';
//                break;
//            default:
//                var_dump($data);
//        }
//
//        if ($exit) {
//            exit;
//        }
//    }
}
