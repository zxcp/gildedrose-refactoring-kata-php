<?php

/** Created by Anton on 02.06.2021. */

declare(strict_types=1);

namespace GildedRose;

use Exception;
use GildedRose\models\AgedBrie;
use GildedRose\models\Backstage;
use GildedRose\models\Conjured;
use GildedRose\models\Dexterity;
use GildedRose\models\Elixir;
use GildedRose\models\Provider;
use GildedRose\models\Sulfuras;

final class ItemsFactory
{
    public const ITEM_NAME_NOT_FOUND = 'Item name not found';

    private array $nameClassMap = [
        AgedBrie::NAME => AgedBrie::class,
        Backstage::NAME => Backstage::class,
        Conjured::NAME => Conjured::class,
        Dexterity::NAME => Dexterity::class,
        Elixir::NAME => Elixir::class,
        Sulfuras::NAME => Sulfuras::class,
    ];

    /**
     * @throws Exception
     */
    public function build(Item &$item): Provider
    {
        $this->isValid($item->name);

        return new $this->nameClassMap[$item->name]($item);
    }

    /**
     * @throws Exception
     */
    private function isValid(string $itemName): bool
    {
        if (! isset($this->nameClassMap[$itemName])) {
            throw new Exception(self::ITEM_NAME_NOT_FOUND);
        }

        return true;
    }
}
