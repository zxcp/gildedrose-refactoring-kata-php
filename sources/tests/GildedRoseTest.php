<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\GildedRose;
use GildedRose\Item;
use GildedRose\ItemsFactory;
use GildedRose\models\AgedBrie;
use GildedRose\models\Backstage;
use GildedRose\models\Dexterity;
use GildedRose\models\Provider;
use GildedRose\models\Sulfuras;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function testFoo(): void
    {
        /** @var Item[] $items */
        $items = [new Item(Dexterity::NAME, 0, 0)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertSame(Dexterity::NAME, $items[0]->name);
    }

    public function testUnknownProvider(): void
    {
        /** @var Item[] $items */
        $items = [new Item('Unknown provider', 0, 0)];
        try {
            (new GildedRose($items))->updateQuality();
        } catch (\Exception $e) {
            $this->assertSame(ItemsFactory::ITEM_NAME_NOT_FOUND, $e->getMessage());
        }
    }

    // После того, как срок храния прошел, качество товара ухудшается в два раза быстрее
    public function testAfterSellInEqualZeroQualityDeterioratesTwice(): void
    {
        $startQuality = 3;
        /** @var Item[] $items */
        $items = [new Item(Dexterity::NAME, 0, $startQuality)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertSame($startQuality - Dexterity::QUALITY_STEP * 2, $items[0]->quality);
    }

    // Качество товара никогда не может быть отрицательным
    public function testQualityCannotBeNegative(): void
    {
        /** @var Item[] $items */
        $items = [new Item(Dexterity::NAME, 0, 0)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->quality);
    }

    // Для товара «Aged Brie» качество увеличивается пропорционально возрасту
    public function testQualityAgedBrieIncreasesWithAge(): void
    {
        /** @var Item[] $items */
        $items = [new Item(AgedBrie::NAME, 0, 0)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertSame(2, $items[0]->quality);
    }

    // Качество товара никогда не может быть больше, чем 50;
    public function testQualityCannotMoreThan50(): void
    {
        /** @var Item[] $items */
        $items = [new Item(AgedBrie::NAME, 0, Provider::MAX_QUALITY)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertSame(Provider::MAX_QUALITY, $items[0]->quality);
    }

    // «Sulfuras» является легендарным товаром, поэтому у него нет срока хранения и не подвержен ухудшению качества;
    public function testSulfuras(): void
    {
        /** @var Item[] $items */
        // Тут по тестам в одном случае sell_in = 0, строкой ниже уже -1
        // Можно замутить тесты на входящие параметы
        $items = [new Item(Sulfuras::NAME, 0, Sulfuras::MAX_QUALITY)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(Sulfuras::MAX_QUALITY, Sulfuras::MAX_QUALITY);
    }

    // Качество «Backstage passes» также, как и «Aged Brie», увеличивается по мере приближения к сроку хранения.
    // Качество увеличивается на 2, когда до истечения срока хранения 10 или менее дней и на 3,
    // если до истечения 5 или менее дней. При этом качество падает до 0 после даты проведения концерта.
    public function testBackstage(): void
    {
        $startQuality = 0;
        /** @var Item[] $items */
        $items = [new Item(Backstage::NAME, 11, $startQuality)];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        // До 11
        $this->assertSame(++$startQuality, $items[0]->quality);
        $gildedRose->updateQuality();
        $startQuality += 2;
        // Меньше 11
        $this->assertSame($startQuality, $items[0]->quality);
        $items[0]->sell_in = 5;
        $gildedRose->updateQuality();
        $startQuality += 3;
        // Меньше 6
        $this->assertSame($startQuality, $items[0]->quality);
        $items[0]->sell_in = 0;
        $gildedRose->updateQuality();
        // Концерт проведён
        $this->assertSame(0, $items[0]->quality);
    }
}
