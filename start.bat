@echo off
cd docker
docker-compose up -d --build
docker-compose exec fpm composer install
cd ..